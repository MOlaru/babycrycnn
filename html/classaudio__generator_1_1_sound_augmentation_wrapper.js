var classaudio__generator_1_1_sound_augmentation_wrapper =
[
    [ "__init__", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a1aa9557af8af6b14606522876dd1702e", null ],
    [ "apply_multiple_transformation", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a857dfad1ea57f41fd61f719bbc5df97f", null ],
    [ "call_sound_wrapper", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a1f75c117f330f9c6e7acd0f8a1cd25a5", null ],
    [ "convert_method_id_from_string", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a46e6efbcab01e0669c3bdc9f4e522396", null ],
    [ "get_method_name", "classaudio__generator_1_1_sound_augmentation_wrapper.html#af86fe9584cb97a076a33860bfaa3534a", null ],
    [ "CALL_NOISE_AUG", "classaudio__generator_1_1_sound_augmentation_wrapper.html#ae8685258f293ab2b80d866c75f513dab", null ],
    [ "CALL_PITCH_AUG", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a04bcf3d2734cfe64b35c938109f662ba", null ],
    [ "CALL_SHIFT_AUG", "classaudio__generator_1_1_sound_augmentation_wrapper.html#aa7632686cfd627986dcb81f7034d8f6f", null ],
    [ "CALL_SPEED_AUG", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a4c8a5b20b97a3cab06794218ab27cd0f", null ],
    [ "MAX_METHOD_ID", "classaudio__generator_1_1_sound_augmentation_wrapper.html#ac0737e90f3131954caae2043cffdce8e", null ],
    [ "METHOD_ARGC", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a19c72db6cb4b062e602e2a32479dd4c6", null ],
    [ "methods_name_map", "classaudio__generator_1_1_sound_augmentation_wrapper.html#af76e13320ed5802ff0a1298aa907a0d3", null ],
    [ "MIN_METHOD_ID", "classaudio__generator_1_1_sound_augmentation_wrapper.html#ac08e72f4cdaecb88d9bac85c7ab73cad", null ],
    [ "shift_dir_map", "classaudio__generator_1_1_sound_augmentation_wrapper.html#a60df49ea0b8c32fefb88da453eb64dc3", null ],
    [ "shift_map_len", "classaudio__generator_1_1_sound_augmentation_wrapper.html#af727d6ec69dd9f69281146053ac04d1c", null ]
];