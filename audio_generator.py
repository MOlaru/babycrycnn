##  \page data_aug Data Augmentation Generator
#   Because for the CNN, we need a large audio data set which sometimes is not available, we can use different methods
# for creating some additional samples from existing one.
#   The methods are:
#       - noise injection: add a random noise to the existing audio file
#       - shifting time: add a random time delay for the audio to start
#       - pitch changing: increase or decrease the intensity of the sound
#       - audio speed change
#       .
#   The data augmentation generator receive a path of an audio file as an input and it create a list with all the sound variation.
#   After that this list can be used to generate spectrograms or for other tasks.
#   \n \n
#   The data augmentation generator is composed from three modules.
#   \n \n
#   
#   First module is a <b>configuration object</b> which has different ranges of values for each function parameter used to modify the audio input.
#   (i.e \c num_files - the number or objects returned in the output list). Each time when a new audio file is generated the methods arguments
#   are randomized in these ranges. (i.e - noise injectation method receive as an argument the \c noise_factor. the values of the noise_factor are randomized between
#   1 and the max value received from the <b>configuration object</b>)
#   \n \n
#
#   The second module is the execution module. This module contain the methods used for sound augmentation. This module contain a list of identifier for each methods.
#   These methods ids are used for the methods factory. For each new file generated, a list of random ids are used. The maximum number of the methods applied at one iteration
#   is set in the configuration object.
#   \n \n
#
#   The last module need to generate the function arguments and to call the factory methods from the second module. For each iteration a random number of transformation are applied to the audio file.
#   The arguments are generated according with the ranges from the configuration object. This module will generate a list of ids and a list of arguments according with the id of methods.
#   \n\n
#   \subsection data_aug Workflow example
#   For the file "./audio/my_audio.wav", a list of 10 files will be generated
#   1st Iteration 
#           - the list of ids, maximum len = 3 [1, 2, 2]
#           - according with list of ids another list of parameters is created
#           - call the factory methods to execute each function from the list of ids
#           - for id=1 - call shift audio, for id=2 - call pitch change methods using the factory object
#           .
#   
#
#

## @package audio_generator Data Audio Augmentation
# This module should be able to receive an audio file and to generate severals augmentated numpy array with version of the initial audio file
#
# More details.

import librosa as librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
import sounddevice as sd
# Size of the FFT, which will also be used as the window length
from datetime import datetime
n_fft = 2048
n_mels = 128
# Step or stride between windows. If the step is smaller than the window lenght, the windows will overlap
hop_length = 512


def get_random_frames(samples, sr, frame_in_sec=2, num_of_frames=5):
    frame_len = frame_in_sec * sr
    list_of_frames = list()
    start_index = np.random.randint(low=0, high=samples.size-frame_in_sec-1, size=num_of_frames)
    for index in range(0, len(start_index)):
        idx = start_index[index]
        list_of_frames.append(samples[idx:idx+frame_len-1])
    return list_of_frames


## This method is used to add a random noise to the audio data.
#  This method return the data with the random noise added.
#  @param data - input audio samples
#  @param noise_factor - this parameter is between 0 and 1
#  \return augmented_data = data + noise_factor*noise
def noise_injection(data, noise_factor):
    if noise_factor == 0:
        return data
    noise = np.random.randn(len(data))
    augmented_data = data + noise_factor*noise
    # Cast back to same data type
    augmented_data = augmented_data.astype(type(data[0]))
    return augmented_data


##  This function is used to add a random delay to the audio. The shift diriction can be to right or to left with a random number of seconds.
#   The audio is padded with silence (samples are modified with 0)
#   @param data - input audio samples
#   @param sampling_rate - value from the initial audio file
#   @param shift_max integer - this value is converted to seconds
#   @param shift_direction string - 'right', 'left', 'both'
def shifting_time(data, sampling_rate, shift_max, shift_direction):

    shift = np.random.randint(sampling_rate * shift_max)
    if shift_direction == 'right':
        shift = -shift
    elif shift_direction == 'both':
        direction = np.random.randint(0, 2)
        if direction == 1:
            shift = -shift
    augmented_data = np.roll(data, shift)
    # Set to silence for heading/ tailing
    if shift > 0:
        augmented_data[:shift] = 0
    else:
        augmented_data[shift:] = 0
    return augmented_data

## Wrapper over the pitch_shift from librosa
#  @param data - input audio samples
#  @param sampling_rate : value of the sampling rate from the initial audio file
#  @param pitch_factor  : integer value >0 for higher frequencies, <0 for lower frequencies, i.e range (-4, 6)
def changing_pitch(data, sampling_rate, pitch_factor):
    # pitch factor integer positive or negative
    return librosa.effects.pitch_shift(data, sampling_rate, pitch_factor)

## Wrapper over the time_stretch from librosa
#  The audio speed is changed, the returned data length is less than the input data
#  @param data - input audio samples
#  @param speed_factor : integer value between [0, 2]
def change_speed(data, speed_factor):
    # speed factor must be a positive number
    return librosa.effects.time_stretch(data, speed_factor)

## Audio Utility faction for calculating the spectrogram
#  The return value is spectrogram database which is used for plotting
#  For the spectrogram calculation we need to compute the short time fourier transform
#  @param y - input audio samples
#  @var n_fft : int > 0 [scalar] the number of point on whitch the fft is calculated
#        length of the windowed signal after padding with zeros.
#        The number of rows in the STFT matrix `D` is (1 + n_fft/2).
#  @var hope_lenght : int > 0 [scalar] number of audio samples between adjacent STFT columns.
#        Smaller values increase the number of columns in `D` without
#        affecting the frequency resolution of the STFT.
#


def calculate_spectrogram(y, sr, hop_length=256, n_fft=2048, n_mels=128):
    spectrogram_librosa = np.abs(librosa.stft(y, n_fft=n_fft, hop_length=hop_length, window='hann')) ** 2
    S_dB = librosa.power_to_db(spectrogram_librosa, ref=np.max)
    return S_dB

## Audio Utility for plotting the spectrom the matplot figure
#  @param fig_index : int or string, the figure id. It is used if you want many figures
#  @param sr        : sample rate (from the initial audio file)
#  @param spectrogram_librosa_db : the 2D array calculate with the \b calculate_spectrogram method
def plot_spectrogram(spectrogram_librosa_db, sr, fig_index, hop_length=512):
    plt.figure(fig_index)
    librosa.display.specshow(spectrogram_librosa_db, sr=sr, hop_length=hop_length)
    plt.tight_layout()



## Generate a unique name for the spectrogram image and save the image to the right directory
#  @param spectrogram_librosa_db : spectrogram values
#  @param sr : sample rate
#  @param img_index : image unique id
#  @param path : store directory
#  @param audio_label : image content label
#  @return img_name : full path image name
def save_spectrogram_img(spectrogram_librosa_db, sr, img_index, path, audio_label, hop_length=256):
    plt.figure(img_index, figsize=(10, 4), frameon=False)
    librosa.display.specshow(spectrogram_librosa_db, sr=sr, fmax=8000)
    img_name = '/img_' + "spec" + '_' + str(img_index) + str(audio_label)
    img_name = path + img_name
    plt.axis('off')
    plt.savefig(img_name)
    plt.close(img_index)
    return img_name + '.png'

## @class SoundAugmentationWrapper 
#  
#  @brief This class is used as a factory for the methods which are creating the augmented data
#  This class contain a list of ids for each method. These are usefull for randomization.
#  The wrapper is used because it is offer the same methods parameters for all the methods involved.
class SoundAugmentationWrapper:
    def __init__(self):
        ## @brief ID Factory for Noise Injection
        self.CALL_NOISE_AUG = 0
        ## @brief ID Factory for Time Shifting
        self.CALL_SHIFT_AUG = 1
        ## @brief ID Factory for Pitch Change
        self.CALL_PITCH_AUG = 2
        ## @brief ID Factory for Speed Change
        self.CALL_SPEED_AUG = 3
        ## @brief used for randomization range of IDs
        self.MAX_METHOD_ID  = 4
        ## @brief used for randomization range of IDs
        self.MIN_METHOD_ID  = 0
        ## @brief String format of the IDs
        self.methods_name_map = dict()
        self.methods_name_map[self.CALL_NOISE_AUG] = "noise"
        self.methods_name_map[self.CALL_SHIFT_AUG] = "shift"
        self.methods_name_map[self.CALL_PITCH_AUG] = "pitch"
        self.methods_name_map[self.CALL_SPEED_AUG] = "speed"
        ## @brief parameter for the direction for shifting in time method
        self.shift_dir_map = ["right", "left", "both"]
        self.shift_map_len = len(self.shift_dir_map)

    ## Return the name of method, based on IDs, this function can be used for debugging or logging
    # @param method_id : integer
    def get_method_name(self, method_id):
        if method_id in self.methods_name_map.keys():
            return self.methods_name_map[method_id]
        else:
            return "error"
    ## This method is the factory used to create the augmented data. The desired method is selected using the method id
    # @param data : input data
    # @param sr : sample rate
    # @param method_idx : method id used for method selection
    # @param factor : intensity of the transformation applied to the audio data
    def call_sound_wrapper(self, data, sr=-1, method_idx=0, factor=0.5):
        aug_data = []
        if method_idx == self.CALL_NOISE_AUG:
            aug_data = noise_injection(data, factor)
        elif method_idx == self.CALL_SHIFT_AUG:
            shift_dir = np.random.randint(low=0, high=self.shift_map_len)
            aug_data = shifting_time(data, sr, factor, self.shift_dir_map[shift_dir])
        elif method_idx == self.CALL_PITCH_AUG:
            aug_data = changing_pitch(data, sr, factor)
        elif method_idx == self.CALL_SPEED_AUG:
            aug_data = change_speed(data, factor)
        return aug_data

    ## This method is used when over the input data is applied a sequence of transformation.
    # @param data : input audio samples
    # @param methods_list_args : list of IDS and other parameters needed 
    def apply_multiple_transformation(self, data, methods_list_args):
        aug = data
        for method_args in methods_list_args:
            aug = self.call_sound_wrapper(aug, method_args[0], method_args[1], method_args[2])
        return aug

    ## Convert the list of string ids to id integer
    # These id strings can be used for contraints
    # @param string_list : list of id strings
    def convert_method_id_from_string(self, string_list):
        list_int_id = []
        for s in string_list:
            list_int_id.append(self.methods_name_map[s])
        return list_int_id

## @class SoundAugmentationConstraints
#  @brief Configuration for the Audio Generator.
#  The class contain the default ranges for parameters for each function from the factory. This Item can be configurated by the USER before calling the generator run function.
#
class SoundAugmentationConstraints:
    ##  Class Constructor
    #   @param sr          : sample rate
    #   @param len_sample  : the length of sample received as an input
    #   @param max_methods : integer maximum number of transformation applied to the audio input
    #   @param noise_range : integer maximum value for the range factor [1, 100]
    #   @param pitch_range : integer 
    #   @param use_methods : list of methods ids allowed 
    #   @param speed_r     : speed factor range
    #
    def __init__(self, sr, len_sample, num_files=10, max_methods=4, noise_range=50, pitch_range=(-3, 6), shift_p =2, speed_r=(1, 3), use_methods=None):
        self.num_files = num_files
        self.noise_range = noise_range
        self.shift_p = shift_p
        self.pitch_range = pitch_range
        self.speed_r = speed_r
        self.num_methods_distribution = [30, 85, 90, 100]
        self.max_num_methods = max_methods
        self.allowed_methods = None
        self.sample_rate = sr
        self.data_size = len_sample
    ## 
    #   Setter for sample rate and data audio length
    def set_sample_rate_and_len(self, sr, data_len):
        self.sample_rate = sr
        self.data_size = data_len
        self.shift_p = 2

## @class SoundAugmentationGenerator
#  @brief Sound Generator for data augumentation. For a specific audio file, it would generate severals variations of the file.
#
#  This classe can generate stimulus for the factory's methods and after that the stimulus are injected to obtain the desired number of augmentation data.
#  
#
class SoundAugmentationGenerator:

    ## Constructor
    # @param cfg_obj is SoundAugmentationConstraints which should be created before
    def __init__(self, cfg_obj):
        ## audio configuration object
        self.generator_cfg = cfg_obj
        ## augumentation factory
        self.audio_factory = SoundAugmentationWrapper()

    ## Generate the number of the transformation for an input file
    # 
    def gen_methods_size(self):
        methods_num = np.random.randint(low=1, high=self.generator_cfg.max_num_methods)
        return methods_num

    ## Generate the list of methods ids for the factory
    #
    def gen_methods_list(self, list_size):
        if self.generator_cfg.allowed_methods is None:
            methods_list = np.random.randint(low=self.audio_factory.MIN_METHOD_ID,
                                             high=self.audio_factory.MAX_METHOD_ID,
                                             size=list_size)
        else:
            list_ids = self.audio_factory.convert_method_id_from_string(self.generator_cfg.allowed_methods)
            methods_list = np.random.randint(low=list_ids,
                                             size=list_size)
        return methods_list

    ## Generate and return a list with arguments for each id from the method list
    #  Each argument is randomized according with the configuration object
    def gen_method_args(self, method_id_list):
        args_factor_list = []
        for method_id in method_id_list:
            if method_id == self.audio_factory.CALL_NOISE_AUG:
                if self.generator_cfg.noise_range != 0:
                    noise_factor = np.random.randint(low=0, high=self.generator_cfg.noise_range)
                    noise_factor /= 100.0
                else:
                    noise_factor = 0
                args_factor_list.append(noise_factor)

            elif method_id == self.audio_factory.CALL_SPEED_AUG:
                speed_factor = np.random.randint(low=self.generator_cfg.speed_r[0], high=self.generator_cfg.speed_r[1])
                args_factor_list.append(speed_factor)

            elif method_id == self.audio_factory.CALL_PITCH_AUG:
                pitch_f = np.random.randint(low=self.generator_cfg.pitch_range[0], high=self.generator_cfg.pitch_range[1])
                args_factor_list.append(pitch_f)

            elif method_id == self.audio_factory.CALL_SHIFT_AUG:
                shift_max = np.random.randint(low=1, high=self.generator_cfg.shift_p)
                args_factor_list.append(shift_max)
            else:
                print("error")
                break
        return args_factor_list

    ## Start the generation for the input data
    #  This method will return a list with augmented audio data
    def run_generator(self, samples):
        aug_list = []
        for file_idx in range(self.generator_cfg.num_files):
            num_methods = self.gen_methods_size()
            methods_ids = self.gen_methods_list(num_methods)
            args = self.gen_method_args(methods_ids)
            # pack args
            wrapper_args = []
            for idx in range(num_methods):
                wrapper_args.append([self.generator_cfg.sample_rate, methods_ids[idx], args[idx]])
            aug = self.audio_factory.apply_multiple_transformation(samples, wrapper_args)
            aug_list.append(aug)
        return aug_list


if __name__ == '__main__':
    y, sr = librosa.load("./audio/1-211527-C-20.wav")

    audio_cfg = SoundAugmentationConstraints(sr=sr, len_sample=len(y), num_files=5, noise_range=10, max_methods=3)

    gen_audio = SoundAugmentationGenerator(audio_cfg)

    audio_aug = gen_audio.run_generator(y)
    idx = 1
    for audio in audio_aug:
        start = datetime.now()
        plot_spectrogram(calculate_spectrogram(audio, sr), sr, idx)
        print(datetime.now() - start)

        #sd.play(audio, sr)
        #status = sd.wait()
        idx += 1
    # y = shifting_time(y, sr, 4, 'left')
    # y = changing_pitch(y, sr, 6)
    # y = change_speed(y, -2)



# Calculate the spectrogram as the square of the complex magnitude of the STFT


    plt.show()
