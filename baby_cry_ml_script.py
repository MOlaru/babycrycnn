import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from pathlib import Path
import librosa
import librosa.display
from librosa.feature import zero_crossing_rate, mfcc, spectral_centroid, spectral_rolloff, spectral_bandwidth,\
    chroma_cens, rms #rms in librpsa 0.7.0, rmse in previous version

from scipy.io import wavfile
import numpy as np
import pandas as pd
from scipy import signal
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer

from keras import Sequential
from keras.layers import Input, Dense, Flatten, Conv2D, Dropout, MaxPooling2D, BatchNormalization, concatenate
from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard

import time

def feature_engineer(audio_data):
    """
    Extract features using librosa.feature.
    Each signal is cut into frames, features are computed for each frame and averaged [mean].
    The numpy array is transformed into a data frame with named columns.
    :param audio_data: the input signal samples with frequency 44.1 kHz
    :return: a numpy array (numOfFeatures x numOfShortTermWindows)
    """

    zcr_feat = compute_librosa_features(audio_data=audio_data, feat_name='zero_crossing_rate')
    rmse_feat = compute_librosa_features(audio_data=audio_data, feat_name='rmse')
    mfcc_feat = compute_librosa_features(audio_data=audio_data, feat_name='mfcc')
    spectral_centroid_feat = compute_librosa_features(audio_data=audio_data, feat_name='spectral_centroid')
    spectral_rolloff_feat = compute_librosa_features(audio_data=audio_data, feat_name='spectral_rolloff')
    spectral_bandwidth_feat = compute_librosa_features(audio_data=audio_data, feat_name='spectral_bandwidth')

    concat_feat = np.concatenate((zcr_feat,
                                  rmse_feat,
                                  mfcc_feat,
                                  spectral_centroid_feat,
                                  spectral_rolloff_feat,
                                  spectral_bandwidth_feat
                                  ), axis=0)

    mean_feat = np.mean(concat_feat, axis=1, keepdims=True).transpose()

    return mean_feat


def compute_librosa_features(audio_data, feat_name, frame_sample=512, sr=44100):
    """
    Compute feature using librosa methods
    :param audio_data: signal
    :param feat_name: feature to compute
    :param frame_sample: feature to compute
    :param sr: sample rate
    :return: np array
    """

    if feat_name == 'zero_crossing_rate':
        return zero_crossing_rate(y=audio_data, hop_length=frame_sample)
    elif feat_name == 'rmse':
        return rms(y=audio_data, hop_length=frame_sample)
    elif feat_name == 'mfcc':
        return mfcc(y=audio_data, sr=sr, n_mfcc=13)
    elif feat_name == 'spectral_centroid':
        return spectral_centroid(y=audio_data, sr=sr, hop_length=frame_sample)
    elif feat_name == 'spectral_rolloff':
        return spectral_rolloff(y=audio_data, sr=sr, hop_length=frame_sample, roll_percent=0.90)
    elif feat_name == 'spectral_bandwidth':
        return spectral_bandwidth(y=audio_data, sr=sr, hop_length=frame_sample)


def get_data(path):
    ''' Returns dataframe with columns: 'path', 'word'.'''
    datadir = Path(path)
    files = [(str(f), f.parts[-2]) for f in datadir.glob('**/*.wav') if f]
    df = pd.DataFrame(files, columns=['path', 'word'])
    return df


def fig2data(fig):
    """
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    # draw the renderer
    fig.canvas.draw()

    # Get the RGBA buffer from the figure
    w, h = fig.canvas.get_width_height()
    buf = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    buf.shape = (h, w, 3)

    # canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
    # buf = np.roll(buf, 3, axis=2)
    return buf


def calculate_spectrogram(y, sr, hop_length=256, n_fft=2048, n_mels=128):
    fig, ax = plt.subplots(figsize=(4, 2), frameon=False)
    plt.axis('off')
    ax.specgram(y, NFFT=n_fft, Fs=sr, noverlap=256)
    rgb_img = fig2data(ax.figure)
    plt.close(ax.figure)
    return rgb_img


def get_spectrogram(p_sample, p_sr):
    tensor_rgb = calculate_spectrogram(p_sample, p_sr)
    return tensor_rgb


def batch_generator(X, y, batch_size=16):
    '''
        Return a random image from X, y
    '''

    while True:
        # choose batch_size random images / labels from the data
        idx = np.random.randint(0, X.shape[0], batch_size)
        im = X[idx]
        label = y[idx]
        spectrogram_list = []
        features_list = []
        for im_path in im:
            audio_samples, sample_rate = librosa.load(im_path, sr=44100)
            spectrogram_list.append(1/255.0 * get_spectrogram(audio_samples, sample_rate))
            features_list.append(feature_engineer(audio_samples))

        yield [np.concatenate([spectrogram_list]), np.concatenate([features_list])], label


def get_model(shape_spectro, shape_audio_features):
    '''Create a keras model.'''
    inputlayer_spectro = Input(shape=shape_spectro)
    inputlayer_audio_features = Input(shape=shape_audio_features)
    spectro_model = BatchNormalization()(inputlayer_spectro)
    spectro_model = Conv2D(16, (3, 3), activation='relu')(spectro_model)
    spectro_model = Dropout(0.25)(spectro_model)
    spectro_model = MaxPooling2D((2, 2))(spectro_model)

    spectro_model = Flatten()(spectro_model)
    spectro_model = Dense(200, activation='relu')(spectro_model)

    audio_features_model = BatchNormalization()(inputlayer_audio_features)
    audio_features_model = Dense(200, activation='relu')(audio_features_model)
    audio_features_model = Flatten()(audio_features_model)
    audio_features_model = Dense(64, activation='relu')(audio_features_model)

    concatenated = concatenate([spectro_model, audio_features_model], axis=-1)
    answer = Dense(100, activation='relu')(concatenated)
    answer = Dense(1, activation='softmax')(answer)

    model = Model(inputs=[inputlayer_spectro, inputlayer_audio_features], outputs=answer)

    return model


# create training and test data.
train = get_data('./small_audio_set')
shape_img = (200, 400, 3)
shape_features = (1, 18)
model = get_model(shape_img, shape_features)
model.compile(loss='binary_crossentropy', optimizer=Adam(), metrics=['accuracy'])

label_binarizer = LabelBinarizer()
X = train.path
y = label_binarizer.fit_transform(train.word)
X, Xt, y, yt = train_test_split(X, y, test_size=0.3, stratify=y)

train_gen = batch_generator(X.values, y, batch_size=4)
valid_gen = batch_generator(Xt.values, yt, batch_size=4)

model.summary()

model.fit_generator(
    generator=train_gen,
    epochs=1,
    steps_per_epoch=10,
    validation_data=valid_gen,
    validation_steps=10,
)


# audio_test, sr_test = librosa.load("small_audio_set/baby_cry/1-60997-B-20.wav", sr=44100)
# my_spec = calculate_spectrogram(audio_test, sr_test)
# plt.imshow(my_spec)
# plt.show()