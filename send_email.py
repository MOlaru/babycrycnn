#import the class definition from "email_handler.py" file

from configparser import ConfigParser
import inspect, os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from message import logger

#Form the absolute path for the settings.ini file
settings_Dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) 
settings_File_Path =  os.path.join(settings_Dir, 'settings.ini')


#================= GET SETTINGS FROM EMAIL SECTION IN settings.ini FILE ==============
def read_Email_Settings():

    try:
        config = ConfigParser()
        config.optionxform=str   #By default config returns keys from Settings file in lower case. This line preserves the case for keys
        config.read(settings_File_Path)

        global FROM_ADD
        global USERNAME
        global PASSWORD
        global SMTP_SERVER
        global SMTP_PORT
        global SEND_TO
        
        SMTP_SERVER = config['EMAIL']['SMTP_ADD']
        SMTP_PORT = config['EMAIL']['SMTP_PORT']
        FROM_ADD = config['EMAIL']['FROM_ADD']
        USERNAME = config['EMAIL']['USERNAME']
        PASSWORD = config['EMAIL']['PASSWORD']
        SEND_TO = config['EMAIL']['SEND_TO']

    except Exception as error_msg:
        print("Error while trying to read SMTP/EMAIL Settings.")
        print("Error {}".format(str(error_msg)))
#=====================================================================================

read_Email_Settings()
logger.info("The email settings are ready")

class Class_eMail:

    def __init__(self):
        logger.debug("Connect to ", SMTP_SERVER, " port ", SMTP_PORT)
        self.session = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
        self.session.ehlo()
        self.session.login(USERNAME, PASSWORD)

    def initialise_Mail_Body(self, To_Add, Subject):
        #Prepare Mail Body
        Mail_Body = MIMEMultipart()
        Mail_Body['From'] = FROM_ADD
        Mail_Body['To'] = To_Add
        Mail_Body['Subject'] = Subject
        return Mail_Body

    #Call this to send plain text emails.
    def send_Text_Mail(self, Subject, txtMessage, To_Add=SEND_TO):
        logger.info("Send Text Email to {}".format(To_Add))
        Mail_Body = self.initialise_Mail_Body(To_Add, Subject)
        #Attach Mail Message
        Mail_Msg = MIMEText(txtMessage, 'plain')
        Mail_Body.attach(Mail_Msg)
        #Send Mail
        self.session.sendmail(FROM_ADD, [To_Add], Mail_Body.as_string())
    
    
    #Call this to send HTML emails.
    def send_HTML_Mail(self, To_Add, Subject, htmlMessage):
        Mail_Body = self.initialise_Mail_Body(To_Add, Subject)
        #Attach Mail Message
        Mail_Msg = MIMEText(htmlMessage, 'html')
        Mail_Body.attach(Mail_Msg)
        #Send Mail
        self.session.sendmail(FROM_ADD, [To_Add], Mail_Body.as_string())
        
if __name__ == '__main__':

    #set the email ID where you want to send the test email 
    To_Email_ID = "alexmhiolaru96@gmail.com"


    # Send Plain Text Email 
    email = Class_eMail()
    email.send_Text_Mail(To_Email_ID, 'Plain Text Mail Subject', 'Hello MIHAI OLARU!')
    del email


    # Send HTML Email
    #email = Class_eMail()
    #email.send_HTML_Mail(To_Email_ID, 'HTML Mail Subject', '<html><h1>This is sample HTML test email body</h1></html>')
    #del email