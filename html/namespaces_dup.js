var namespaces_dup =
[
    [ "Workflow example", "data_aug.html#data_aug", null ],
    [ "audio_generator", "namespaceaudio__generator.html", null ],
    [ "baby_cry_ml_script", "namespacebaby__cry__ml__script.html", null ],
    [ "doc_mainpage", "namespacedoc__mainpage.html", null ],
    [ "load_model", "namespaceload__model.html", null ],
    [ "sound_mix_script", "namespacesound__mix__script.html", null ],
    [ "spectro_script", "namespacespectro__script.html", null ],
    [ "tag_files", "namespacetag__files.html", null ]
];