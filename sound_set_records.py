import librosa as librosa
import sounddevice as sd
import matplotlib.pyplot as plt
import numpy as np
import librosa.display
import os
import datetime
from audio_generator import save_spectrogram_img, calculate_spectrogram, plot_spectrogram
from sound_microphone import MicrophoneDrive

static_id = 0
label_id = dict()

label_id["y"] = "baby_cry"
label_id['n'] = "noise"

def setup_phase(path_dir, labels_list):
    if os.path.isdir(path_dir) == 0:
        os.mkdir(path_dir)
    for label in labels_list:
        class_dir = os.path.join(path_dir, label)
        if os.path.isdir(class_dir) == 0:
            os.mkdir(class_dir)

def get_uid():
    global static_id
    static_id += 1
    now = datetime.datetime.now()
    ret_id = str(static_id) + now.strftime("%Y%m%d%H_%M_%S")
    return ret_id


def save_spectro(librosa_db, sr, key, path_dir):
    global label_id, static_id
    img_path = ""
    if key in label_id.keys():
        path = os.path.join(path_dir, label_id[key])
        img_path = save_spectrogram_img(librosa_db, path=path, img_index=static_id, audio_label=get_uid(), sr=sr)
    return img_path


if __name__ == '__main__':
    # frame_length = 512
    sound_duration = 2
    rec_sample_rec = 44100
    frames = sound_duration*rec_sample_rec
    path_dir = "./test_set2"
    setup_phase(path_dir, label_id.values())
    print("Press the letter to choose the right class")
    for key, value in label_id.items():
        print("Press {} for class {}".format(key, value))
    print("Insert the class label")
    k = input()
    print("The recording is started")
    while 1:
        # rec_audio = sd.rec(frames=frames, samplerate=rec_sample_rec, channels=1, blocking=True)
        # rec_audio = rec_audio.reshape(frames, )
        mic = MicrophoneDrive(threshold=50, chunk_size=1024, rate=44100, silence=0, audio_duration=2)
        rec_audio = mic.record_from_microphone()
        db = calculate_spectrogram(rec_audio, rec_sample_rec)
        # plot_spectrogram(db, rec_sample_rec, static_id)
        # plt.show()
        path = save_spectro(db, rec_sample_rec, k, path_dir)
        print("Save Image to ", path)
