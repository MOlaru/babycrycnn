import numpy as np
from tensorflow.keras.preprocessing import image
from tensorflow.keras.models import load_model
from audio_generator import calculate_spectrogram as spectro
from audio_generator import save_spectrogram_img as save_spectro
import sound_microphone as s_mic
from message import logger
import send_email
import os
import time

from scipy.interpolate import interp1d


def apply_transfer(signal, transfer, interpolation='linear'):
    constant = np.linspace(-1, 1, len(transfer))
    interpolator = interp1d(constant, transfer, interpolation)
    return interpolator(signal)


# hard limiting
def limiter(x, treshold=0.8):
    transfer_len = 1000
    transfer = np.concatenate([ np.repeat(-1, int(((1-treshold)/2)*transfer_len)),
                                np.linspace(-1, 1, int(treshold*transfer_len)),
                                np.repeat(1, int(((1-treshold)/2)*transfer_len)) ])
    return apply_transfer(x, transfer)


# smooth compression: if factor is small, its near linear, the bigger it is the
# stronger the compression
def arctan_compressor(x, factor=2):
    constant = np.linspace(-1, 1, 1000)
    transfer = np.arctan(factor * constant)
    transfer /= np.abs(transfer).max()
    return apply_transfer(x, transfer)


class CryPrediction:

    def __init__(self, path_model):
        self.index_img = 1
        self.runme_dir = "./runme"
        self.img_path = os.path.join(self.runme_dir, "imgs")
        self.model = load_model(path_model)
        3
        self.microphone = s_mic.MicrophoneDrive(rate=44100, threshold=500, chunk_size=2048)
        # self.class_labels = ["baby_cry", "baby_laughing", "noise", "silence"]
        self.class_labels = ["baby_cry", "noise"]
        self.target_size = (400, 200)
        self.email = send_email.Class_eMail()
        self.setup()

    def setup(self):
        if os.path.isdir(self.runme_dir) == 0:
            os.mkdir(self.runme_dir)
            os.mkdir(self.img_path)
        else:
            for root, dirs, files in os.walk(self.runme_dir, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            if os.path.isdir(self.img_path) == 0:
                os.mkdir(self.img_path)

    def run_prediction(self):
        logger.info("Record sound from microphone")
        sound = self.microphone.record_from_microphone()
        sound = sound / np.abs(sound).max()  # x scale between -1 and 1
        x3 = arctan_compressor(sound)
        x3 = x3 * 32767
        s_db = spectro(x3, self.microphone.RATE)
        spectrogram_path = save_spectro(s_db, self.microphone.RATE, img_index=1, path=self.img_path, audio_label="record")
        self.index_img += 1
        s_img = image.load_img(spectrogram_path, target_size=self.target_size)
        s_pixel = image.img_to_array(s_img)
        s_pixel = np.expand_dims(s_pixel, axis=0)

        s_pixel /= 255.
        preds = self.model.predict(s_pixel)
        logger.info("Load Image {}".format(spectrogram_path))
        self.print_prediction(preds)
        if self.class_detection(preds, 0):
            logger.info("Class {} is detected".format(self.class_labels[0]))
            logger.info("Send Email Notification")
            # time.sleep(5)
            # self.email.send_Text_Mail(Subject='Alerta Plans copil', txtMessage='Va plange copilul!!!')

    def print_prediction(self, probabilities):
        for index in range(0, len(probabilities[0])):
            logger.info("Class {} = {}".format(self.class_labels[index], probabilities[0][index]))

    def class_detection(self, preds, class_id, level=0.75):
        ret_val = 0
        if preds[0][class_id] > level:
            ret_val = 1
        return ret_val


if __name__ == '__main__':
    detect_cry = CryPrediction('./models/baby_cry3_after.h5')
    logger.info('Run the Baby Crying Recognition')
    while 1:
        detect_cry.run_prediction()


