var hello__word__ml_8py =
[
    [ "BATCH_SIZE", "hello__word__ml_8py.html#a941172f1d059d95bef14b7f2d67ad780", null ],
    [ "cmap", "hello__word__ml_8py.html#aef0aef3664b0198722e98209b70563a4", null ],
    [ "DATA_URL", "hello__word__ml_8py.html#aacd3ff0bebfb50998e8251942ff4c56a", null ],
    [ "digit", "hello__word__ml_8py.html#a35633f407665052883e012cd132faa8a", null ],
    [ "epochs", "hello__word__ml_8py.html#ad609d7cd6034a9e776fa0eda86fa082c", null ],
    [ "loss", "hello__word__ml_8py.html#aa55e5397f39fd7a19ad17d6246a289a5", null ],
    [ "metrics", "hello__word__ml_8py.html#a4e715c14539d293cc1cfbf2dc53b7b56", null ],
    [ "model", "hello__word__ml_8py.html#a3891c8a2992a35d7ce95c57b4d2521ec", null ],
    [ "optimizer", "hello__word__ml_8py.html#afb90021df503ceaee7e8ca1b27a3300d", null ],
    [ "path", "hello__word__ml_8py.html#a4277ef508a60a9b3ab85da342c575b5c", null ],
    [ "SHUFFLE_BUFFER_SIZE", "hello__word__ml_8py.html#a6e07fa1d96532223ddff339c4c2aae3f", null ],
    [ "test_acc", "hello__word__ml_8py.html#aedf3b20949886c0c1956aaca07466c9b", null ],
    [ "test_dataset", "hello__word__ml_8py.html#a03d559f53838d053cccb0a49f683b96c", null ],
    [ "test_examples", "hello__word__ml_8py.html#aa80e6553693ee7f2eabe1509ace6ef42", null ],
    [ "test_labels", "hello__word__ml_8py.html#a4bfebbd64b79cd999c6596139153c37a", null ],
    [ "test_loss", "hello__word__ml_8py.html#aefd7e59b368f1eebf581676a57b2666c", null ],
    [ "train_dataset", "hello__word__ml_8py.html#a99bfab0bf261e55af9703baf5b50a2e1", null ],
    [ "train_examples", "hello__word__ml_8py.html#a85beda450ab19c85c1cf47689a82722f", null ],
    [ "train_labels", "hello__word__ml_8py.html#a63f0524f771e306c110a3cccc7af1272", null ]
];