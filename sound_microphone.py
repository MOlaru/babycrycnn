from sys import byteorder
from array import array
from struct import pack

import pyaudio
import numpy as np


class MicrophoneDrive:

    def __init__(self, threshold=500, chunk_size=1024, rate=22050, silence=0, audio_duration=2):
        self.THRESHOLD = threshold
        self.CHUNK_SIZE = chunk_size
        self.FORMAT = pyaudio.paInt16
        self.RATE = rate
        self.MAXIMUM = 16384
        self.SILENCE = silence
        self.SOUND_FRAMES = self.RATE * audio_duration

    def is_silent(self, snd_data):
        """Returns 'True' if below the 'silent' threshold"""
        return max(snd_data) < self.THRESHOLD

    def normalize(self, snd_data):
        "Average the volume out"
        times = float(self.MAXIMUM)/max(abs(i) for i in snd_data)

        r = array('h')
        for i in snd_data:
            r.append(int(i*times))
        return r

    def trim(self, snd_data):
        "Trim the blank spots at the start and end"
        def _trim(snd_data):
            snd_started = False
            r = array('h')

            for i in snd_data:
                if not snd_started and abs(i) > self.THRESHOLD:
                    snd_started = True
                    r.append(i)
                elif snd_started:
                    r.append(i)
            return r

        # Trim to the left
        snd_data = _trim(snd_data)

        # Trim to the right
        snd_data.reverse()
        snd_data = _trim(snd_data)
        snd_data.reverse()
        return snd_data

    def add_silence(self, snd_data, seconds):
        if seconds == 0:
            return snd_data
        "Add silence to the start and end of 'snd_data' of length 'seconds' (float)"
        silence = [0] * int(seconds * self.RATE)
        r = array('h', silence)
        r.extend(snd_data)
        r.extend(silence)
        return r

    def record(self):
        """
        Record a word or words from the microphone and
        return the data as an array of signed shorts.

        Normalizes the audio, trims silence from the
        start and end, and pads with 0.5 seconds of
        blank sound to make sure VLC et al can play
        it without getting chopped off.
        """
        p = pyaudio.PyAudio()
        stream = p.open(format=self.FORMAT, channels=1, rate=self.RATE,
                        input=True, output=True, frames_per_buffer=self.CHUNK_SIZE)

        num_silent = 0
        snd_started = False

        r = array('h')
        frames = self.CHUNK_SIZE
        while 1:
            # little endian, signed short
            snd_data = array('h', stream.read(self.CHUNK_SIZE, False))
            if byteorder == 'big':
                snd_data.byteswap()
            r.extend(snd_data)

            silent = self.is_silent(snd_data)

            if silent and snd_started:
                num_silent += 1
            elif not silent and not snd_started:
                snd_started = True
            if snd_started:
                frames += self.CHUNK_SIZE
                if frames >= self.SOUND_FRAMES:
                    break

        sample_width = p.get_sample_size(self.FORMAT)
        stream.stop_stream()
        stream.close()
        p.terminate()

        r = self.normalize(r)
        r = self.trim(r)
        r = self.add_silence(r, self.SILENCE)
        return sample_width, r

    def record_from_microphone(self, numpy_data=float):
        """Records from the microphone and outputs the resulting data to 'path'"""
        sample_width, data = self.record()
        s_data = np.array(data)
        s_data = np.fromiter(s_data, dtype=numpy_data)
        return s_data


if __name__ == '__main__':
    import sound_mix_script as util
    from matplotlib import pyplot as plt
    print("please speak a word into the microphone")
    mic = MicrophoneDrive()
    sound = mic.record_from_microphone()
    util.compute_spectrogram(sound, mic.RATE)
    plt.show()
    print("done - result written to demo.wav")
