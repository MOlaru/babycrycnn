var searchData=
[
  ['n_5ffft_86',['n_fft',['../namespaceaudio__generator.html#a73c708afae34312ea524efec60618eb3',1,'audio_generator']]],
  ['name_87',['name',['../namespacetag__files.html#af59d8bc12525ddedd335e44dcffbeb93',1,'tag_files']]],
  ['noise_5finjection_88',['noise_injection',['../namespaceaudio__generator.html#a49bd7aa883cdbe85ae6dabeef06e3ae8',1,'audio_generator']]],
  ['noise_5frange_89',['noise_range',['../classaudio__generator_1_1_sound_augmentation_constraints.html#abf2480c72661a4ef9abaebe6e028e97b',1,'audio_generator::SoundAugmentationConstraints']]],
  ['num_5ffiles_90',['num_files',['../classaudio__generator_1_1_sound_augmentation_constraints.html#aa59f7fce64c8b8c340d1a57ed866a82c',1,'audio_generator.SoundAugmentationConstraints.num_files()'],['../classspectro__script_1_1_baby_sound_set.html#ab25676b8d3f0342d3af1429555f8cb9b',1,'spectro_script.BabySoundSet.num_files()']]],
  ['num_5fmethods_5fdistribution_91',['num_methods_distribution',['../classaudio__generator_1_1_sound_augmentation_constraints.html#aa9b5587f464ca32f9750dccbeb0ba6c5',1,'audio_generator::SoundAugmentationConstraints']]],
  ['num_5fof_5fvalidation_92',['num_of_validation',['../classspectro__script_1_1_baby_sound_set.html#a388fd0063644771bc92b28b7a7eb03ba',1,'spectro_script::BabySoundSet']]]
];
