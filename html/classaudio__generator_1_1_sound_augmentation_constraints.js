var classaudio__generator_1_1_sound_augmentation_constraints =
[
    [ "__init__", "classaudio__generator_1_1_sound_augmentation_constraints.html#a1507cdc584681a431ee9cbe49b791436", null ],
    [ "set_sample_rate_and_len", "classaudio__generator_1_1_sound_augmentation_constraints.html#a1d60c2c34308686cccdfecaa370b8b61", null ],
    [ "allowed_methods", "classaudio__generator_1_1_sound_augmentation_constraints.html#ad38fd52a8f3bfede4383008f7942bdb0", null ],
    [ "data_size", "classaudio__generator_1_1_sound_augmentation_constraints.html#a9710798c8fe6ece606c11bfc9cef86bc", null ],
    [ "max_num_methods", "classaudio__generator_1_1_sound_augmentation_constraints.html#a586d94850d1b55ce4886f08dbabdde40", null ],
    [ "noise_range", "classaudio__generator_1_1_sound_augmentation_constraints.html#abf2480c72661a4ef9abaebe6e028e97b", null ],
    [ "num_files", "classaudio__generator_1_1_sound_augmentation_constraints.html#aa59f7fce64c8b8c340d1a57ed866a82c", null ],
    [ "num_methods_distribution", "classaudio__generator_1_1_sound_augmentation_constraints.html#aa9b5587f464ca32f9750dccbeb0ba6c5", null ],
    [ "pitch_range", "classaudio__generator_1_1_sound_augmentation_constraints.html#ae56ecc98c9232cc1f92735b4f898b1d3", null ],
    [ "sample_rate", "classaudio__generator_1_1_sound_augmentation_constraints.html#aac864339fd9c1bbebda36c080084047b", null ],
    [ "shift_p", "classaudio__generator_1_1_sound_augmentation_constraints.html#a1eb93e1905aa4dd0d71bfaf75cc670a6", null ],
    [ "speed_r", "classaudio__generator_1_1_sound_augmentation_constraints.html#af8f000269b1b0fe38c272692c3ce1c77", null ]
];