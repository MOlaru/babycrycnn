import os
import shutil


def get_data_label(filename):
    filename = filename.decode("utf-8")
    filename = filename.split(".")
    filename = filename[0]
    filename = filename.split('-')
    audio_label = int(filename[-1])
    return audio_label


small_dir_path = r"./small_audio_set"
big_dir_path = r"./audio"

if os.path.isdir(small_dir_path) == 0:
    os.mkdir(small_dir_path)
else:
    # clean folder
    dir = os.fsencode(small_dir_path)
    files = os.listdir(dir)
    for f in files:
        os.remove(os.path.join(small_dir_path, f))

label_list = [0, 5, 20, 903, 901]
# self.label_map[0] = "noise"
# self.label_map[5] = "noise"
# self.label_map[20] = "baby_cry"
# self.label_map[903] = "baby_laugh"
# self.label_map[901]]

directory = os.fsencode(big_dir_path)
files = os.listdir(directory)

for f in files:
    label = get_data_label(f)
    if label in label_list:
        f_name = f.decode("utf-8")
        src_path = os.path.join(big_dir_path, f_name)
        dest_path = os.path.join(small_dir_path, f_name)
        shutil.copy(src_path, dest_path)
