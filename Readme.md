# Baby cry detection using CNN

Create a neural network modules which should detect the baby cry using sound spectrograms interpretation

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project use Python 3.7.3
Please install this Python version on your machine.
Make sure that the pip is available in the PATH.


### Installing

A step by step series of examples that tell you how to get a development env running

1) Please install python 3.7.3

2) Create your own repository
```
git clone  https://MOlaru@bitbucket.org/Anilatac/disertatie.git
cd disertatie
```

3) Create your python work environment in the folder repository

```
mkdir <DIR>
python -m venv <DIR>
```
4) Source your environment

Linux Users:
```
source <DIR>/bin/activate
```

Windows Users. Open a cmd line (power shell) as Administrator. Go to the repo's folder.
```
Set-ExecutionPolicy RemoteSigned
```
```
<DIR>/Scripts/activate
```
```
Set-ExecutionPolicy Restricted
```

Now the environment is activated

5) Run the pip to bring all the required modules:
```
pip install -r ./py_requirments.txt
```

## Running the tests
TODO


### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system


## Authors

* **Mihai Olaru** - *Worker :D*
* **Catalina Sarbu** - *Worker :D*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
