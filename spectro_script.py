## \mainpage Baby Cry official documentation
#  
#  \section intro_sec Introduction
#  The main objective for this project is to classify an audio data received as an input to be part of a specific class.
#   \n\n
#  The categories in this case are:
#       - baby_cry 
#       - baby_laugh 
#       - noise
#       - silence
#       .
#  These categories should be enaugh for a smart device to do some action if baby is crying and is nobody around.
#  If the baby cry sound is detected, the mother can be notified.
#  \n\n
#  The development steps:
#       - find a data set with audio files. All files should have the same sample rate and the same duration.
#       - process the audio files, extract the features and encapsulate the information to tensors
#       - the features are images with spectrograms (Until now)
#       - create the Neural Network architecture and train the model and save the weights
#       - load the model, record with the microphone an audio, generate the spectrogram and call the prediction function from the model
#       .
#  \section data_set Data Set
#  For the data set we can use the Spectrogram Generation and the Data Augmentation Generation. From the files name should be extracted the label of category.
#  The data set is generated from the audio directory. We are using wav format for the audio files, where the duration of file is 5 seconds. For data set is used
#  Environment Sound Classification data set - ESC 50. The labels are configurated in the Spectrogram Generation module.(i.e for file \b 1-211527-A-20.wav, the label is equal with 20 and it means baby cry).
#  The data set with images is generated in the following directory structure:
#       - data_set_imgs
#           - train (training data folder)
#               - baby_cry (folder with spectrogram images from baby cry category)
#               - baby_laugh
#               - noise
#               - silence
#               .
#           - valid (validation/test data folder)
#               - baby_cry (folder with spectrogram images from baby cry category)
#               - baby_laugh
#               - noise
#               - silence
#               .
#       .
#  \n
#  The validation data set and the training set should be distinct.
#  Download the full data set from $ git clone https://github.com/giulbia/baby_cry_detection.git .
#  \n\n
#  The data input for the neural network is a set of images generated with the \b spectro_script.py.
#  This script need to receive the path to the audio folder and a path where the images will be saved according with the directory structure presented before.
#  The desired label can be configured from the constructor of BabySound Class. (i.e \c self.label_map[0] = "noise", this means that all spectrograms from the files with name *_0.wav are stored in noise directory)
#  \n
#  Each image has a unique name.
#  \section Main
#
#  \subsection spectro Spectrogram Generation
#   Spectrogram Image example for baby crying. For spectrogram generation we use \b librosa python package.
#  \n 
#  \image html img_MainThread_1baby_cry.png
#  \subsection cnn Convolutional Neural Networks
#   The architecture structure can be found in \c baby_cry_ml_script.py. To train the cnn, we are using Tensor Flow python library. The input for the cnn is the data set with spectrograms which is normalized 
#  (float values between 0 and 1). For the cnn we can use different types of layers:
#       - Conv2D -> convolutional network layer with kernel
#       - MaxPooling2D -> this is used to reduce the number of neural connections
#       - Dropout -> discard random neural weights
#       - Dense -> fully connected layers with neurons
#       .
#   \n
#   The activation function used is ReLU. This is used for non-linearity and the last layer, which has the number of neurons equal with the number of category classe, has Sigmoid for function activation.
#   After the training the model is saved to a file. This file will be later uploaded on the RPI device. For a new image received as an input the cnn model will return a vector with probabilities for each category.
#
#  \subsection start Get Started
#   Project Structure:
#       - audio_generator.py    : used for data augumentation
#       - spectro_script.py     : spectrogram images generator
#       - baby_cry_ml_script.py : cnn model, train the model, save the model
#       - load_model.py         : use the saved model and make prediction for the recorded sound
#       .
#   \n
#   To create a data set with Images:
#       - \c Instantiate the BabySoundSet with audio_dir and image_path. 
#       - Check the \c label_map from constructure. 
#       - Call \c get_data_set_list() method
#       .
#   \n
#   To create a validation set after the training set generation:
#       - Call \c generate_all_validation_set() method
#       .
#   By default the spectro_script.py has incorporated the data augmentation generator. If you want to modify the number of data augmented for each audio file, you can modify the constraint object from spectro_script.py:
#       - <tt> self.gen_cfg = audio_util.SoundAugmentationConstraints(1, 1, num_files=15, noise_range=3, max_methods=3) </tt>
#       - Check the constraint object documentation for more details
#       .
#   To run a training for the cnn: run the baby_cry_ml_script.py. 
#   \n
#   To check the model and to record sounds from the laptop microphone use the load_model.py.
#
#  \subsection rpi_module RPI Module
#   TODO
#  \subsection py_module Python Modules
#  Before starting the project we need to install the following modules:
#   - numpy - array handler and random generation
#   - matplotlib - ploting spectrograms
#   - librosa - audio processing
#   - tensorflow (based on keras) - cnn training
#   - sounddevice - sound recording
#   .
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.io.wavfile as wav
from datetime import datetime
import os
import shutil
from threading import current_thread # SOLUTION
import concurrent
from concurrent.futures import ThreadPoolExecutor
import logging
import librosa as librosa
import librosa.display
import audio_generator as audio_util

from audio_generator import save_spectrogram_img

logging.basicConfig(
    format='%(threadName)s %(levelname)s: %(message)s',
    level=logging.INFO
)

## @class BabySoundSet
#  @brief This class is used to convert the audio files from the audio directory to spectrogram images. In additional, it can be used the data augmentation generator to generate even more data training.
#  Initially, all images should be stored in CSV file format and after that this CSV is used to train the model.\n
#  <b>The CSV file format is not used anymore !!!!!!! because the generated file is huge and it is a better way to do it saving the spectrograms</b> 
#  <b>!!!!! Another functionality which is not used it MIX SOUND - generate spectrograms to mixed sounds (i.e baby cry and rain sound) </b>
#  \n
#  The images directory is generated using a multiplethreads abordation. One thread need to plot the spectrogram and to save it to the right directory and the other threads should read the *.wav files and to calculate the SFFT and the spectrogram.
#  The thread communication is made using the queue (Message Passing Architecture).
#  The default number of threads can be configurated from /c max_workers. For thread handlin, we are using the \b ThreadPoolExecutor from concurrent.futures.
#  For image saving, we are using just one thread because matplotlib has difficulties with multithreading. It is better to use just one thread because we want to have integrity for the training set.
#
class BabySoundSet:

    def __init__(self, data_set_path, spec_img_w=10, spec_img_h=4, num_files=None, save_to=None, path_imgs=None, valid=3, max_workers=4, gen_disable=0):
        ## audio directory
        self.dir_path = data_set_path
        ## Used for CSV
        self.img_w = spec_img_w
        ## Used for CSV
        self.img_h = spec_img_h
        ## label separator in the wav file name
        self.separator = '-'
        ## Used for CSV
        self.num_files = num_files
        ## Used for CSV
        self.data_set_path = save_to
        ## Used for CSV
        self.label_c_name = '"Audio Label"'
        ## Used for CSV
        self.data_c_name = "Data"
        ## Used for CSV
        self.file_data = None
        ## Used for CSV
        self.tensor = None
        ## Used for CSV
        self.img_pixel_w = 155
        ## Used for CSV
        self.img_pixel_h = 154
        ## Spectrogram Images folder
        self.img_set_dir = path_imgs
        ## Used for CSV
        self.current_image = None
        ## Map from label integer to label string
        self.label_map = dict()
        ## Directory Structure for each label <main_dir>/<label_name>
        self.label_dir = dict()
        ## Used for sound MIX
        self.label_mix_sound = dict()
        if self.img_set_dir is not None:
            self.train_path = self.img_set_dir + "/train"
            self.valid_path = self.img_set_dir + "/valid"

        ## Number of files moved for the generation set
        self.num_of_validation = valid
        ## Set sound for mixing
        self.audio_set_files = dict()
        ## Data Augmentation Generator
        self.gen_cfg = audio_util.SoundAugmentationConstraints(1, 1, num_files=15, noise_range=5, max_methods=3)
        ## Data Augmentation Generator
        self.generator = audio_util.SoundAugmentationGenerator(self.gen_cfg)
        self.gen_disable = gen_disable
        ## Number of Threads used
        self.max_workers = max_workers
        self.thread_name_prefix = 'T_'
        ## Thread pool
        self.pool = ThreadPoolExecutor(max_workers=max_workers,  thread_name_prefix='T')
        ## Thread Message Passing Queue
        self.thread_queue = []
        self.thread_queue.append([])
        self.thread_queue.append([])
        ## Flag Thread Job Done
        self.thread_done = 0
        ## Parameter for spectrogram
        self.hop_length = 512
        ## Map of thread ids used to count where they sent the last spectrogram values
        # This is used for duty distribution
        self.thread_id_map = dict()
        self.setup_completed = 0
        print("Sound setup is completed")

    ## map label initialization
    def init_label_map(self):
        self.label_map[42] = "noise"
        self.label_map[37] = "noise"
        self.label_map[20] = "baby_cry"

        for key in self.label_map.keys():
            self.audio_set_files[key] = []

        for idx in range(self.max_workers):
            self.thread_id_map[self.thread_name_prefix + str(idx)] = 0

        # mix sound with this
        # self.label_mix_sound[36] = []  # vacuum cleaner
        # self.label_mix_sound[19] = []  # thunderstorm - 19

        # self.label_mix_sound[37] = []  # clock_alarm - 37
        # self.label_mix_sound[16] = []  # wind - 16d
        # self.label_mix_sound[30] = []  # door_wood_knock - 30
        # self.label_mix_sound[10] = []  # rain - 10
        # self.label_mix_sound[25] = []  # footsteps - 25

    ## setup phase function
    #  delete the last generated data set if it exist and create the directory structure
    def setup_phase(self):
        self.setup_completed = 1
        self.init_label_map()
        if self.file_data is not None:
            # write the csv header
            self.file_data = open(self.data_set_path, "w")
            header = self.label_c_name + "," + self.data_c_name + '\n'
            # self.file_data.write(header)

        if self.img_set_dir is not None:
            if os.path.isdir(self.img_set_dir) == 0:
                os.mkdir(self.img_set_dir)
            if os.path.isdir(self.train_path) == 0:
                os.mkdir(self.train_path)
            if os.path.isdir(self.valid_path) == 0:
                os.mkdir(self.valid_path)
            filelist = [f for f in os.listdir(self.train_path)]
            for f in filelist:
                current_dir = os.path.join(self.train_path, f)
                files_in_dir = [f1 for f1 in os.listdir(current_dir)]
                for f2 in files_in_dir:
                    os.remove(os.path.join(current_dir, f2))
                os.rmdir(os.path.join(current_dir))

            filelist = [f for f in os.listdir(self.valid_path)]
            for f in filelist:
                current_dir = os.path.join(self.valid_path, f)
                files_in_dir = [f1 for f1 in os.listdir(current_dir)]
                for f2 in files_in_dir:
                    os.remove(os.path.join(current_dir, f2))
                os.rmdir(os.path.join(current_dir))

            for key in self.label_map.keys():
                self.label_dir[key] = os.path.join(self.train_path, self.label_map[key])
                try:
                    os.mkdir(self.label_dir[key])
                    os.mkdir(os.path.join(self.valid_path, self.label_map[key]))
                except:
                    pass

    ## Thread Duty 
    # Plot the spectrograms and save the images in the right subdirectory according with the audio label
    # @param t_queue_idx : specify the thread queue is we are using multiple threads for image storing
    def store_img(self, t_queue_idx):
        it_file = 0
        while True:
            if self.thread_done == 1 and len(self.thread_queue[t_queue_idx]) == 0:
                break
            if len(self.thread_queue[t_queue_idx]) != 0:
                spectr = self.thread_queue[t_queue_idx].pop(0)
                # samples, sample_rate
                audio_label = spectr[2]
                save_spectrogram_img(spectr[0], spectr[1], it_file, self.label_dir[audio_label], audio_label)
                it_file += 1
                logging.info(f"Save one image to {self.label_dir[audio_label]}")
        return 1

    ##  Thread Duty
    #   Decode the file name, extract the label, calculate the spectrogram and send the results to the thread queue for image saving
    #   Additional duty : generate the data augmentation
    #   @param file : audio file path
    def data_set_processing(self, file):
        filename = os.fsdecode(file)
        audio_label = self.get_data_label(filename)
        # file name handle
        full_path = self.dir_path + '/' + filename
        name = current_thread().name
        t_list_id = self.thread_id_map[name]
        # self.thread_id_map[name] = ~self.thread_id_map[name]
        if audio_label in self.label_map.keys():
            logging.info(f"Process {full_path} with audio label = {audio_label}")
            samples, sample_rate = librosa.load(full_path)

            # set-up generator
            if not self.gen_disable:
                self.gen_cfg.set_sample_rate_and_len(sample_rate, len(samples))

            # Save spectrogram images
            if self.img_set_dir is not None:
                # pass samples for plot

                # split the audio file in chunks

                audio_chunks = audio_util.get_random_frames(samples=samples, sr=sample_rate, num_of_frames=15)
                for audio in audio_chunks:
                    librosa_db = audio_util.calculate_spectrogram(samples, sample_rate)
                    self.thread_queue[t_list_id].append([librosa_db, sample_rate, audio_label])
                    self.audio_set_files[audio_label].append(full_path)

                    # # generate other files
                    if not self.gen_disable:
                        audio_aug_list = self.generator.run_generator(samples)
                        for audio_aug in audio_aug_list:
                            librosa_db = audio_util.calculate_spectrogram(audio_aug, sample_rate)
                            self.thread_queue[t_list_id].append([librosa_db, sample_rate, audio_label])
            # Duties
            if self.file_data is not None:
                self.tensor = self.get_spectrogram_tensor()
                self.write_to_file(audio_label)

            logging.info(f"FINISHED FILE {filename}")

        if audio_label in self.label_mix_sound.keys():
            self.label_mix_sound[audio_label].append(full_path)
            print("File ", full_path, " is found for mixing")
        return 1
    ## MainThread Duty
    #  read the directory path for audio file and call the thread pool for job splitting.
    #  
    def get_data_set_list(self):
        self.setup_phase()

        # generate the data set
        directory = os.fsencode(self.dir_path)
        files = os.listdir(directory)
        t_save_spectr0 = self.pool.submit(self.store_img, 0)
        # t_save_spectr1 = self.pool.submit(self.store_img, 1)

        t_results = [self.pool.submit(self.data_set_processing, file) for file in files]

        # self.pool.map(self.data_set_processing, files)
        # stub
        for t in concurrent.futures.as_completed(t_results):
            t.result()

        self.thread_done = 1

        t_save_spectr0.result()
        # t_save_spectr1.result()

        # self.mix_and_save_img(it_file)
        self.close_phase()

    ## mix sounds and save the spectrogram. This function is used for data diversity.
    # 
    def mix_and_save_img(self, start_idx):
        print("Start to generate the mixed sounds")
        img_idx = start_idx
        for label_1, original_sound in self.audio_set_files.items():
            self.audio_label = label_1
            print("Generate mixed sound for  ", self.label_map[label_1])
            for sound in original_sound:
                sample_rate, samples_sound = wav.read(sound)
                for label2, mix_sound in self.label_mix_sound.items():
                    for mix_with in mix_sound:
                        samples_rate, samples_mix = wav.read(mix_with)
                        mix_factor = np.random.random()
                        if mix_factor > 0.5:
                            mix_factor = 1.0 - mix_factor
                        sound_result = (1.0 - mix_factor) * samples_sound + mix_factor * samples_mix
                        save_spectrogram_img(img_index=img_idx, path=self.label_dir[self.audio_label])
                        plt.close()
                        img_idx += 1
        print("Mixed Sounds are generated with SUCCESS!")

    ## This function is called after the data set genration is finished
    # The validation set generation is called
    def close_phase(self):
        if self.file_data is not None:
            self.file_data.close()
            print("The CSV file is generated")

        if self.img_set_dir is not None:
            self.generate_all_validation_set()

    ## CSV FILE
    def write_to_file(self, audio_label):
        # default - csv format
        row = str(audio_label) + r' '
        for tensor_idx in range(0, self.tensor.size - 1):
            # for tensor_idx in range(0, 5):
            row += str(self.tensor[0][tensor_idx]) + " "
        row += str(self.tensor[0][5]) + "\n"
        self.file_data.write(row)

    ## Extract label from file name
    def get_data_label(self, filename):
        filename = filename.split(".")
        filename = filename[0]
        filename = filename.split(self.separator)
        audio_label = int(filename[-1])
        return audio_label

    ## CSV FILE
    def get_spectrogram_tensor(self):

        self.img_pixel_w = self.current_image.shape[1]
        self.img_pixel_h = self.current_image.shape[0]
        tensor = self.current_image.reshape(1, -1)

        return tensor

    ## Split each category subdirectories to the Threads Pool for validation set
    #
    def generate_all_validation_set(self):
        if self.setup_completed == 0:
            self.setup_completed = 1
            self.init_label_map()

        sub_dir = []
        for k, v in self.label_map.items():
            if v not in sub_dir:
                sub_dir.append(v)

        t_results = [self.pool.submit(self.generate_valid_set, s) for s in sub_dir]

        # self.pool.map(self.data_set_processing, files)
        # stub
        for t in concurrent.futures.as_completed(t_results):
            t.result()

    # Generate the validation set from the training directory
    def generate_valid_set(self, label_dir):
        print("Choose validation set for ", label_dir)
        train_dir = os.fsencode(self.train_path + "/" + label_dir)
        train_img_list = os.listdir(train_dir)
        if len(train_img_list) < self.num_of_validation:
            move_file_cnt = int(len(train_img_list) / 4)
        else:
            move_file_cnt = self.num_of_validation

        for idx in range(move_file_cnt):
            # move images from train folder to valid folder
            file_index = np.random.randint(low=0, high=len(train_img_list))
            file_name = train_img_list.pop(file_index).decode()
            src_file = self.train_path + '/' + label_dir + '/' + file_name
            dst_file = self.valid_path + '/' + label_dir + '/' + file_name

            # remove file name from map

            # cut the file from train dir to valid dir
            shutil.copyfile(src_file, dst_file)
            # delete the file from train folder
            os.remove(src_file)


if __name__ == '__main__':
    start = datetime.now()
    audio_set = BabySoundSet(data_set_path="./audio", path_imgs="./spectro_baby_set", valid=700)
    # audio_set.get_data_set_list()
    audio_set.generate_all_validation_set()
    print(datetime.now() - start)
